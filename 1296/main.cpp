// 1296. Гиперпереход 
#include <cstdio>
#include <algorithm>

int main(){
    int N, a;
    scanf("%d", &N);

    int sum = 0;
    int sumMax = 0;

    for(int i = 0; i < N; i++){
        scanf("%d", &a);
        sum += a;
        sumMax = std::max(sum, sumMax);
        sum = std::max(sum, 0);
    }
    printf("%d", sumMax);
    
    return 0;
}
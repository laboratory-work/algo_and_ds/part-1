// 1155. Дуоны
#include <cstdio>

int main(){
    int a, b, c, d, e, f, g, h;

    scanf("%d %d %d %d %d %d %d %d", &a, &b, &c, &d, &e, &f, &g, &h);

    if ((a + h + f + c) != (b + g + e + d)){
        printf("IMPOSSIBLE");
    }else{
        for (int i = 0; i < 100; i++){
            if (a > 0 && b > 0){
                printf("AB-\n");
                a--;
                b--;
            }

            if (a > 0 && d > 0){
                printf("AD-\n");
                a--;
                d--;
            }

            if (a > 0 && e > 0){
                printf("AE-\n");
                a--;
                e--;
            }

            if (a > 0 && g > 0){
                printf("FB+\n");
                printf("FG-\n");
                printf("AB-\n");
                a--;
                g--;
            }

            if (h > 0 && e > 0){
                printf("HE-\n");
                h--;
                e--;
            }

            if (h > 0 && d > 0){
                printf("HD-\n");
                h--;
                d--;
            }

            if (h > 0 && g > 0){
                printf("HG-\n");
                h--;
                g--;
            }

            if (h > 0 && b > 0){
                printf("FG+\n");
                printf("HG-\n");
                printf("FB-\n");
                h--;
                b--;
            }

            if (f > 0 && b > 0){
                printf("FB-\n");
                f--;
                b--;
            }

            if (f > 0 && g > 0){
                printf("FG-\n");
                f--;
                g--;
            }

            if (f > 0 && e > 0){
                printf("FE-\n");
                f--;
                e--;
            }

            if (f > 0 && d > 0){
                printf("HG+\n");
                printf("HD-\n");
                printf("FG-\n");
                f--;
                d--;
            }

            if (c > 0 && b > 0){
                printf("CB-\n");
                c--;
                b--;
            }

            if (c > 0 && d > 0){
                printf("CD-\n");
                c--;
                d--;
            }

            if (c > 0 && g > 0){
                printf("CG-\n");
                c--;
                g--;
            }

            if (c > 0 && e > 0){
                printf("HG+\n");
                printf("EH-\n");
                printf("CG-\n");
                c--;
                e--;
            }
        }
    }
    return 0;
}

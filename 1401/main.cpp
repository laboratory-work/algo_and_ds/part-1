#include <cstdio>

int num = 3;

int a[512][512];

void fill(int size, int shift_x, int shift_y, int hole_x, int hole_y){
    if(size == 2){
        for (int i = shift_x; i < shift_x + 2; i++) {
            for (int j = shift_y; j < shift_y + 2; j++) {
                if (i != hole_x || j != hole_y) {
                    a[i][j] = num++ / 3;
                }
            }
        } 
        return;
    }

    int half_size = size >> 1;

    for (int i = 0; i < 2; i++) {
        int cur_x = shift_x + i * half_size;
        for (int j = 0; j < 2; j++) {
            int cur_y = shift_y + j * half_size;
            if(cur_x > hole_x || hole_x >= cur_x + half_size || cur_y > hole_y || hole_y >= cur_y + half_size){
                a[shift_x + half_size + i - 1][shift_y + half_size + j - 1] = num++ / 3;
            }
        }
    }

    for(int i = 0; i < 2; i++){
        int cur_x = shift_x + i * half_size;
        for(int j = 0; j < 2; j++){
            int cur_y = shift_y + j * half_size;
            if(cur_x <= hole_x && hole_x < cur_x + half_size 
                && cur_y <= hole_y && hole_y < cur_y + half_size){
                fill(half_size, cur_x, cur_y, hole_x, hole_y);
            }else{
                fill(half_size, cur_x, cur_y, shift_x + half_size-1 + i, shift_y + half_size-1 + j);
            }
        }
    }

}

void printArr(int size){
    for (int i = 0; i < size; ++i){
        for (int j = 0; j < size; ++j){
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
}

int main(){
    int N, x, y;
    scanf("%d", &N);
    scanf("%d %d", &x, &y);
    int size = 1 << N;
    x--;y--;

    fill(size, 0, 0, x, y);

    printArr(size);
    return 0;
}

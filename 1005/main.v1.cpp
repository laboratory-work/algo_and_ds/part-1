#include <cstdio>
#include <vector>
#include <climits>
#include <algorithm>
#include <cstdlib>

int main() {
    int n;
    scanf("%d", &n);

    std::vector<int> a(n);

    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }
    int minDiff = INT_MAX;
    for (int code = 0; code < (1 << (n-1)); ++code) {
        int sum0 = 0;
        int sum1 = 0;
        for(int i = 0; i < n; i++){
            if(((code >> i) & 1) == 0){
                sum0 += a[i];
            }else {
                sum1 += a[i];
            }
        }
        minDiff = std::min(minDiff, std::abs(sum0 - sum1));
    }
    printf("%d", minDiff);
    return 0;
}

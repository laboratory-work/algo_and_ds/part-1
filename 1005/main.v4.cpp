//1005. Куча камней
#include <cstdio>
#include <bitset>

int main() {
    int n;
    int sum = 0;
    scanf("%d", &n);

    std::bitset<20*100000+1> sumSet;
    sumSet[0] = true;

    for (int i = 0; i < n; ++i) {
        int a;
        scanf("%d", &a);
        sumSet |= sumSet << a;
        sum += a;
    }
    int minDiff = sum;

    for (int i = 0; i < sum; ++i) {
        if(sumSet[i]) {
            minDiff = std::min(minDiff, std::abs(sum - i - i));
        }
    }

    printf("%d", minDiff);
    return 0;
}
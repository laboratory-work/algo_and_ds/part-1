#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cassert>

int main() {
    int n;
    scanf("%d", &n);

    std::vector<int> a(n);

    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }

    std::vector<int> group(n, 0);
    int sum0 = 0;
    int sum1 = 0;
    for (int i = 0; i < n; ++i) {
        sum0 += a[i];
    }
    int minDiff = std::abs(sum0 - sum1);

    while(true){
        int i = n - 1;
        while(group[i] == 1){
            group[i] = 0;
            sum1 -= a[i];
            sum0 += a[i];
            i--;
        }
        if (i == 0){
            break;
        }
        assert(group[i] == 0);
        group[i] = 1;
        sum0 -= a[i];
        sum1 += a[i];
        minDiff = std::min(minDiff, std::abs(sum0 - sum1));
    }

    printf("%d", minDiff);
    return 0;
}
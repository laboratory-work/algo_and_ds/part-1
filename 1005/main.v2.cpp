#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstdlib>

int minDiff(int i, int sum0, int sum1, const std::vector<int> &a) {
    if(i == -1) {
        return std::abs(sum0 - sum1);
    }else{
        return std::min(minDiff(i-1, sum0+a[i], sum1, a), minDiff(i-1, sum0, sum1+a[i], a));
    }
}

int main() {
    int n;
    scanf("%d", &n);

    std::vector<int> a(n);

    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }

    printf("%d", minDiff(n-2, a[n-1], 0, a));
    return 0;
}